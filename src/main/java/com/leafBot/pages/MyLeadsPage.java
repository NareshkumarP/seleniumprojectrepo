package com.leafBot.pages;

import com.leafBot.testng.api.base.Annotations;

public class MyLeadsPage extends Annotations {
	
	public CreateLeadPage clickCreateLead() {
		driver.findElementByLinkText("Create Lead")
		.click();
		return new CreateLeadPage();
		
	}
	public FindLeadPage clickFindLead() {
		driver.findElementByLinkText("Find Leads").click();
		return new FindLeadPage();
		
	}
}
