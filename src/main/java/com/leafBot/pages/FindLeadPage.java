package com.leafBot.pages;

import com.leafBot.testng.api.base.Annotations;

public class FindLeadPage extends Annotations {
 public FindLeadPage enterFName(String fName) {
	 driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(fName);
	 return this;
	 }
 public FindLeadPage clickFindLeads() {
	 driver.findElementByXPath("//button[text() = 'Find Leads']").click();
	 return this;
 }
 
 public ViewLeadPage clickLeadName(String fName) {
	 driver.findElementByXPath("(//a[text()='"+fName+"'])[1]").click();
	 return new ViewLeadPage();
 }
 
}
