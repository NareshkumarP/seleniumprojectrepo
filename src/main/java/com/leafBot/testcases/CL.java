package com.leafBot.testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.leafBot.pages.*;
import com.leafBot.testng.api.base.Annotations;
import com.leafBot.selenium.api.base.*;

// Annotations == ProjectBase
public class CL extends Annotations{
	
	@BeforeTest
      public void setData() {
		testcaseName = "TC001_LoginAndLogout";
    	testcaseDec = "Login into leaftaps";
    	author = "Naresh";
    	category = "sanity";
		excelFileName = "TC001";
	} 

    @Test(dataProvider="fetchData") 
	public void loginAndLogout(String uName, String pwd) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin();
//		//.clickLogout();
		
	}
	
	@BeforeClass
	public void setDataforCreateLead() {
		excelFileName = "TC002";
	}
	
	@Test(dataProvider="fetchData")
	public void createLeadTest(String cName, String fName, String lName) {
		new MyHomePage()
		.clickLeadsTab()
		.clickCreateLead()
		.typeCompanyName(cName)
		.typeFirstName(fName)
		.typeLastName(lName)
		.clickCreateLeadButton()
		.verifyFirstName();
	}
	
	
	
}






